# Developing the Data Infrastructure for Analyzing COVID-19 Related Wearable Device Data

**CovIdentify** is a project from the Duke University's BIGS IDEAS Lab. This is a Capstone project that is in partnership with Duke University's [MIDS program](https://datascience.duke.edu/). 

**Team Members:**

Alena Kalodzitsa

Joseph Hsieh

 Xiao Lu

## Purpose

The ultimate purpose of this project is to be able to detect COVID-19 and influenza using wearable and mobile devices. This repository is a subsection of The CovIdentify project

## Goal

The goal of our team was to establish an automated ETL (Extract,  Transform, Load) Pipeline that extracts raw wearable data (from Fitbit and Garmin APIs), transforms the data, and loads it to the PostgreSQL database (*Covidentify Analysis Database*) in Microsoft Azure. This repository is a mirror of the code that exists within Azure's ML Studio. The ETL Pipeline enables researchers to easily query data from *Covidentify Analysis Database* to use it for modeling.

**About CovIdentify**: CovIdentify is an ongoing study initiated by Duke University’s BIG IDEAS lab (PI: Dr. Jessilyn Dunn, affiliated with the BME and Biostatistics & Bioinformatics Departments), IRB Protocol Number: 2020-0412. The study has multiple teams; programmers working on platform implementation, community outreach team increasing participant enrollment, PhD students focusing on different areas of biomarker marker development, and data pipeline development team.

